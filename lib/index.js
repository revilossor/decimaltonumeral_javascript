const generate = require('./roman-numeral-generator')
const args = process.argv.splice(2)

args.forEach(arg => {
  console.log(`${arg} in roman numerals is ${generate(arg)}`)
})
