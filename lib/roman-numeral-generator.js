const numeralObjects = [
  { numeral: 'M', value: 1000 },
  { numeral: 'CM', value: 900 },
  { numeral: 'D', value: 500 },
  { numeral: 'CD', value: 400 },
  { numeral: 'C', value: 100 },
  { numeral: 'XC', value: 90 },
  { numeral: 'L', value: 50 },
  { numeral: 'XL', value: 40 },
  { numeral: 'X', value: 10 },
  { numeral: 'IX', value: 9 },
  { numeral: 'V', value: 5 },
  { numeral: 'IV', value: 4 },
  { numeral: 'I', value: 1 }
]

/*
  Go through each object in the array, from highest value to lowest.
  When the object has a value that the number is greater than ( or equal to )
  return that object

  subtract the objects value from the number and concat the numeral to the numeral string

  repeat until the number is zero, then return the numeral string

  This assumes the array is ordered from highest value to lowest,
  an there is an object for the value 1
*/

module.exports = function generate (number, numeral = '') {
  if (number === 0) { return numeral } // exit condition
  const object = numeralObjects.find(item => number >= item.value) // get object
  return generate(number - object.value, `${numeral}${object.numeral}`) // recurse
}
