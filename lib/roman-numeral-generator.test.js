const generator = require('./roman-numeral-generator')

describe('generates the correct roman numeral', () => {
  const assertions = [
    { decimal: 1, numeral: 'I' },
    { decimal: 5, numeral: 'V' },
    { decimal: 10, numeral: 'X' },
    { decimal: 20, numeral: 'XX' },
    { decimal: 3999, numeral: 'MMMCMXCIX' }
  ]

  assertions.forEach(assertion => {
    it(`converts ${assertion.decimal} to ${assertion.numeral}`, () => {
      expect(generator(assertion.decimal)).toBe(assertion.numeral)
    })
  })
})
